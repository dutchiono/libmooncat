(ns libmooncat.image.png
  (:require
   [libmooncat.accessory.palette :as palette]
   [libmooncat.image.pixels :as pixels]
   [libmooncat.util :as util]
   ;;#?(:cljs ["crc-32" :as crc32])
   [clojure.string :as str])
  #?(:clj
     (:import
      [java.nio ByteBuffer]
      [java.util.zip CRC32])))

;;;; Util

(defn- crc [ba]
  #?(:clj (let [crc (CRC32.)]
            (.update crc ^"[B" ba)
            (.getValue crc))
     :cljs (throw (ex-info "crc32 not available" {})) #_(.buf crc32 ba)))

(defn str->bytes [s]
  #?(:clj
     (map byte s)
     :cljs
     (for [i (range (count s))]
       (.charCodeAt s i))))

(defn ->bytes
  "Takes a sequence of specs which are of the form `[:type-kw value]`
  and concatenates them into a Big Endian byte array.

  Possible types are:
  `:int8`  - single byte integer
  `:int16` - two byte integer
  `:int32` - four byte integer
  `:bytes` - byte array
  "
  [specs]
  (let [total-len (reduce (fn [l [type-key value]]
                            (+ l
                               (case type-key
                                 :int8  1
                                 :int16 2
                                 :int32 4
                                 :bytes (alength ^"[B" value))))
                          0 specs)]
    #?(:clj
       (let [ba (byte-array total-len)
             buf (ByteBuffer/wrap ba)]
         (doseq [[type-key value] specs]
           (case type-key
             :int8 (.put buf (byte (if (< 127 value) (- value 256) value)))
             :int16 (.putShort buf (short value))
             :int32 (.putInt buf (int (if (< Integer/MAX_VALUE value) (- value (* 2 Integer/MAX_VALUE) 2) value)))
             :bytes (.put buf ^"[B" value)))
         ba)
       :cljs
       (let [ba (js/ArrayBuffer. total-len)
             buf (js/DataView. ba)]
         (reduce (fn [offset [type-key value]]
                   (+ offset
                      (case type-key
                        :int8  (do (.setInt8 buf offset value false) 1)
                        :int16 (do (.setInt16 buf offset value false) 2)
                        :int32 (do (.setInt32 buf offset value false) 4)
                        :bytes (do
                                 (dotimes [i (alength value)]
                                   (.setInt8 buf (+ offset i) (aget value i) false))
                                 (alength value)))))
                 0
                 specs)
         (js/Uint8Array. ba)))))



;;;; PNG Chunks

(def ^:private png-header-spec
  [[:int8 137]
   [:int8 80]
   [:int8 78]
   [:int8 71]
   [:int8 13]
   [:int8 10]
   [:int8 26]
   [:int8 10]])


(defn- generate-png-chunk-spec [type-code data]
  (let [[tc0 tc1 tc2 tc3] (str->bytes type-code)

        data-len (alength ^"[B" data)
        crc-len (+ 4 data-len)
        total-len (+ 4 4 data-len 4)
        crc-data (->bytes
                  [[:int8 tc0]
                   [:int8 tc1]
                   [:int8 tc2]
                   [:int8 tc3]
                   [:bytes data]])
        crc-value (crc crc-data)]
    [[:int32 data-len]
     [:int8 tc0]
     [:int8 tc1]
     [:int8 tc2]
     [:int8 tc3]
     [:bytes data]
     [:int32 crc-value]]))

(def ^:private png-footer-spec
  (generate-png-chunk-spec
   "IEND"
   (->bytes [])))

(defn- generate-IHDR-chunk-spec [width height]
  (generate-png-chunk-spec
   "IHDR"
   (->bytes
    [[:int32 width]
     [:int32 height]
     [:int8 8] ; bit depth
     [:int8 3] ; color type
     [:int8 0] ; compression method
     [:int8 0] ; filter method
     [:int8 0] ; interlace method
     ])))

(defn- generate-PLTE-chunk-spec [palette-color-triples]
  (generate-png-chunk-spec
   "PLTE"
   (->bytes
    (reduce (fn [triples [r g b]]
              (conj triples
                    [:int8 r]
                    [:int8 g]
                    [:int8 b]))
            []
            palette-color-triples))))

(defn- generate-IDAT-chunk-spec [compressed-pixel-data]
  (generate-png-chunk-spec
   "IDAT"
   compressed-pixel-data))

(defn- generate-tRNS-chunk-spec [alphas-vector]
  (generate-png-chunk-spec
   "tRNS"
   (->bytes
    (mapv #(vector :int8 %) alphas-vector))))

(defn generate-png [width height palette idat scale]
  (let [scale (if (and (int? scale) (pos? scale))
                       scale
                       1)

        pixel-bytes (if (string? idat)
                      (util/hex->byte-array idat)
                      idat)
        pixel-byte-array (if (== scale 1)
                           pixel-bytes
                           (:pixel-byte-array (pixels/scale-pixel-bytes
                                               {:width width :height height :pixel-byte-array pixel-bytes}
                                               scale)))
        palette (into [[0 0 0 0]] palette)
        palette-colors (map #(take 3 %) palette)
        palette-alphas (map #(nth % 3) palette)
        png-spec (vec
                  (concat
                   png-header-spec
                   (generate-IHDR-chunk-spec width height)
                   (generate-PLTE-chunk-spec palette-colors)
                   (generate-tRNS-chunk-spec palette-alphas)
                   (generate-IDAT-chunk-spec pixel-byte-array)
                   png-footer-spec))]
    (->bytes png-spec)))

(defn generate-png-uri [width height palette idat scale]
  (str "data:image/png;base64," (util/byte-array->base64 (generate-png width height palette idat scale))))

(defn generate-accessory-png [cat-id {:keys [width height palette idat]} {:keys [scale to-uri]}]
  (let [get-color (palette/get-full-palette cat-id)
        palette (map #(:rgba (get-color %)) palette)
        png-bytes (generate-png width height palette idat scale)]
    (if to-uri
      (str "data:image/png;base64," (util/byte-array->base64 png-bytes))
      png-bytes)))
