(ns libmooncat.image.generator.browser-png-uri)

(defn generate-png-uri [width height {:keys [glow-color
                                             glow-size
                                             background-glow
                                             background-pixels
                                             foreground-glow
                                             foreground-pixels]}
                        scale padding background-color]
  (let [canvas (js/document.createElement "canvas")
        ctx (.getContext canvas "2d")
        set-pixels! (fn [pixels]
                      (doseq [pixel pixels]
                        (set! (.-fillStyle ctx) (pixel 2))
                        (.fillRect ctx
                                   (+ padding (* scale (pixel 0)))
                                   (+ padding (* scale (pixel 1)))
                                   scale scale)))]
    (set! (.-width canvas) (+ (* width scale) (* 2 padding)))
    (set! (.-height canvas) (+ (* height scale) (* 2 padding)))
    (when background-color
      (set! (.-fillStyle ctx) background-color)
      (.fillRect ctx 0 0 (.-width canvas) (.-height canvas)))
    (.save ctx)
    (set! (.-shadowBlur ctx) glow-size)
    (set! (.-shadowColor ctx) glow-color)
    (set-pixels! background-glow)
    (.restore ctx)
    (set-pixels! background-pixels)

    (.save ctx)
    (set! (.-shadowBlur ctx) glow-size)
    (set! (.-shadowColor ctx) glow-color)
    (set-pixels! foreground-glow)
    (.restore ctx)
    (set-pixels! foreground-pixels)
    (.toDataURL canvas)))
