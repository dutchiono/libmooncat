(ns libmooncat.image.generator.buffered-image
  (:require
   [clojure.java.io :as io])
  (:import
   [com.jhlabs.image GaussianFilter]
   [java.awt Color Graphics2D RenderingHints]
   [java.awt.geom AffineTransform]
   [java.awt.image BufferedImage ConvolveOp Kernel]
   [javax.imageio ImageIO]))

(defn ^BufferedImage draw-pixels! [^BufferedImage img padding scale pixels]
  (let [padding (int padding)
        scale (int scale)
        g2d (doto (.createGraphics img)
              (.setRenderingHint RenderingHints/KEY_RENDERING RenderingHints/VALUE_RENDER_QUALITY)
              (.setRenderingHint RenderingHints/KEY_ANTIALIASING RenderingHints/VALUE_ANTIALIAS_OFF))]
    (doseq [[x y color] pixels]
      (.setColor g2d (Color. (int (color 0)) (int (color 1)) (int (color 2)) (int (color 3))))
      (.fillRect g2d (+ padding (* scale x)) (+ padding (* scale y)) scale scale))
    (.dispose g2d)
    img))

(defn ^BufferedImage draw-monochrome-pixels! [^BufferedImage img padding scale pixels ^Color color]
  (let [padding (int padding)
        scale (int scale)
        g2d (doto (.createGraphics img)
              (.setRenderingHint RenderingHints/KEY_RENDERING RenderingHints/VALUE_RENDER_QUALITY)
              (.setRenderingHint RenderingHints/KEY_ANTIALIASING RenderingHints/VALUE_ANTIALIAS_OFF))]
    (.setColor g2d color)
    (doseq [[x y] pixels]
      (.fillRect g2d (+ padding (* scale x)) (+ padding (* scale y)) scale scale))
    (.dispose g2d)
    img))

(defn ^BufferedImage generate-buffered-image [width height {:keys [glow-color
                                                                   glow-size
                                                                   background-glow
                                                                   background-pixels
                                                                   foreground-glow
                                                                   foreground-pixels]}
                                              scale padding background-color]
  (let [blur (GaussianFilter. glow-size)
        width (int (+ (* scale width) (* 2 padding)))
        height (int (+ (* scale height) (* 2 padding)))
        glow-color (Color. (int (glow-color 0)) (int (glow-color 1)) (int (glow-color 2)) (int (glow-color 3)))
        result (BufferedImage. width height BufferedImage/TYPE_INT_ARGB)
        result-foreground-glow (BufferedImage. width height BufferedImage/TYPE_INT_ARGB)
        background-glow-img (draw-monochrome-pixels! (BufferedImage. width height BufferedImage/TYPE_INT_ARGB)
                                                     padding scale background-glow glow-color)
        foreground-glow-img (draw-monochrome-pixels! (BufferedImage. width height BufferedImage/TYPE_INT_ARGB)
                                                     padding scale foreground-glow glow-color)]
    (when-not (empty? background-glow) (.filter blur background-glow-img result))
    (draw-pixels! result padding scale background-pixels)
    (when-not (empty? foreground-glow)
      (.filter blur foreground-glow-img result-foreground-glow)
      (let [g2d (.createGraphics result)]
        (.drawImage g2d result-foreground-glow (AffineTransform.) nil)
        (.dispose g2d)))

    (draw-pixels! result padding scale foreground-pixels)
    result))

(defn save-png! [^BufferedImage img filename]
  (ImageIO/write img "png" (io/as-file filename)))




;;
