(ns libmooncat.accessory.palette
  (:require
   [clojure.string :as str]
   [libmooncat.data.color :as color]
   [libmooncat.util :as util]))


(def ^:private base-colors
  [{:id 0   :rgba [255 255 255   0] :title "Transparent Background"}
   {:id 1   :rgba [255 255 255 255] :title "White"}
   {:id 2   :rgba [212 212 212 255] :title "Pale Grey"}
   {:id 3   :rgba [170 170 170 255] :title "Light Grey"}
   {:id 4   :rgba [128 128 128 255] :title "Grey"}
   {:id 5   :rgba [ 85  85  85 255] :title "Dark Grey"}
   {:id 6   :rgba [ 42  42  42 255] :title "Deep Grey"}
   {:id 7   :rgba [  0   0   0 255] :title "Black"}
   {:id 8   :rgba [249 134 134 255] :title "Light Red"}
   {:id 9   :rgba [242  13  13 255] :title "Red"}
   {:id 10  :rgba [161   8   8 255] :title "Dark Red"}
   {:id 11  :rgba [249 178 134 255] :title "Light Orange"}
   {:id 12  :rgba [242 101  13 255] :title "Orange"}
   {:id 13  :rgba [161  67   8 255] :title "Dark Orange"}
   {:id 14  :rgba [249 220 134 255] :title "Light Gold"}
   {:id 15  :rgba [242 185  13 255] :title "Gold"}
   {:id 16  :rgba [161 123   8 255] :title "Dark Gold"}
   {:id 17  :rgba [249 249 134 255] :title "Light Yellow"}
   {:id 18  :rgba [242 242  13 255] :title "Yellow"}
   {:id 19  :rgba [161 161   8 255] :title "Dark Yellow"}
   {:id 20  :rgba [210 249 134 255] :title "Light Chartreuse"}
   {:id 21  :rgba [166 242  13 255] :title "Chartreuse"}
   {:id 22  :rgba [110 161   8 255] :title "Dark Chartreuse"}
   {:id 23  :rgba [134 249 134 255] :title "Light Green"}
   {:id 24  :rgba [ 13 242  13 255] :title "Green"}
   {:id 25  :rgba [  8 161   8 255] :title "Dark Green"}
   {:id 26  :rgba [134 249 205 255] :title "Light Teal"}
   {:id 27  :rgba [ 13 242 154 255] :title "Teal"}
   {:id 28  :rgba [  8 161 103 255] :title "Dark Teal"}
   {:id 29  :rgba [134 249 249 255] :title "Light Cyan"}
   {:id 30  :rgba [ 13 242 242 255] :title "Cyan"}
   {:id 31  :rgba [  8 161 161 255] :title "Dark Cyan"}
   {:id 32  :rgba [134 205 249 255] :title "Light Sky Blue"}
   {:id 33  :rgba [ 13 154 242 255] :title "Sky Blue"}
   {:id 34  :rgba [  8 103 161 255] :title "Dark Sky Blue"}
   {:id 35  :rgba [134 134 249 255] :title "Light Blue"}
   {:id 36  :rgba [ 13  13 242 255] :title "Blue"}
   {:id 37  :rgba [  8   8 161 255] :title "Dark Blue"}
   {:id 38  :rgba [182 134 249 255] :title "Light Indigo"}
   {:id 39  :rgba [108  13 242 255] :title "Indigo"}
   {:id 40  :rgba [ 72   8 161 255] :title "Dark Indigo"}
   {:id 41  :rgba [210 134 249 255] :title "Light Purple"}
   {:id 42  :rgba [166  13 242 255] :title "Purple"}
   {:id 43  :rgba [110   8 161 255] :title "Dark Purple"}
   {:id 44  :rgba [235 134 249 255] :title "Light Violet"}
   {:id 45  :rgba [215  13 242 255] :title "Violet"}
   {:id 46  :rgba [144   8 161 255] :title "Dark Violet"}
   {:id 47  :rgba [249 134 210 255] :title "Light Pink"}
   {:id 48  :rgba [242  13 166 255] :title "Pink"}
   {:id 49  :rgba [161   8 110 255] :title "Dark Pink"}
   {:id 50  :rgba [ 65  22  22 255] :title "Deep Red"}
   {:id 51  :rgba [ 65  54  22 255] :title "Deep Yellow"}
   {:id 52  :rgba [ 43  65  22 255] :title "Deep Green"}
   {:id 53  :rgba [ 22  65  48 255] :title "Deep Teal"}
   {:id 54  :rgba [ 22  33  65 255] :title "Deep Blue"}
   {:id 55  :rgba [ 43  22  65 255] :title "Deep Purple"}
   {:id 56  :rgba [ 65  22  54 255] :title "Deep Pink"}
   {:id 57  :rgba [236 198 198 255] :title "Pale Red"}
   {:id 58  :rgba [236 221 198 255] :title "Pale Yellow"}
   {:id 59  :rgba [202 236 198 255] :title "Pale Green"}
   {:id 60  :rgba [198 236 236 255] :title "Pale Teal"}
   {:id 61  :rgba [198 217 236 255] :title "Pale Blue"}
   {:id 62  :rgba [217 198 236 255] :title "Pale Purple"}
   {:id 63  :rgba [236 198 226 255] :title "Pale Pink"}
   {:id 64  :rgba [ 56  43  31 255] :title "Umber"}
   {:id 65  :rgba [ 72  47  25 255] :title "Mocha"}
   {:id 66  :rgba [101  62  29 255] :title "Cinnamon"}
   {:id 67  :rgba [130  79  35 255] :title "Brown"}
   {:id 68  :rgba [153  96  46 255] :title "Peanut"}
   {:id 69  :rgba [184 132  86 255] :title "Tortilla"}
   {:id 70  :rgba [218 192 169 255] :title "Beige"}
   {:id 71  :rgba [255 255 255 200] :title "White Glass"}
   {:id 72  :rgba [212 212 212 200] :title "Pale Grey Glass"}
   {:id 73  :rgba [170 170 170 200] :title "Light Grey Glass"}
   {:id 74  :rgba [128 128 128 200] :title "Grey Glass"}
   {:id 75  :rgba [ 85  85  85 200] :title "Dark Grey Glass"}
   {:id 76  :rgba [ 42  42  42 200] :title "Deep Grey Glass"}
   {:id 77  :rgba [  0   0   0 200] :title "Black Glass"}
   {:id 78  :rgba [242  13  13 200] :title "Vibrant Red Smoked Glass"}
   {:id 79  :rgba [108  19  19 200] :title "Dull Red Smoked Glass"}
   {:id 80  :rgba [242 185  13 200] :title "Vibrant Yellow Smoked Glass"}
   {:id 81  :rgba [108  86  19 200] :title "Dull Yellow Smoked Glass"}
   {:id 82  :rgba [128 242  13 200] :title "Vibrant Green Smoked Glass"}
   {:id 83  :rgba [ 64 108  19 200] :title "Dull Green Smoked Glass"}
   {:id 84  :rgba [ 13 242 154 200] :title "Vibrant Teal Smoked Glass"}
   {:id 85  :rgba [ 19 108  74 200] :title "Dull Teal Smoked Glass"}
   {:id 86  :rgba [ 13  70 242 200] :title "Vibrant Blue Smoked Glass"}
   {:id 87  :rgba [ 19  41 108 200] :title "Dull Blue Smoked Glass"}
   {:id 88  :rgba [127  13 242 200] :title "Vibrant Purple Smoked Glass"}
   {:id 89  :rgba [ 64  19 108 200] :title "Dull Purple Smoked Glass"}
   {:id 90  :rgba [242  13 185 200] :title "Vibrant Pink Smoked Glass"}
   {:id 91  :rgba [108  19  86 200] :title "Dull Pink Smoked Glass"}
   {:id 92  :rgba [242  13  13 128] :title "Vibrant Red Stained Glass"}
   {:id 93  :rgba [108  19  19 128] :title "Dull Red Stained Glass"}
   {:id 94  :rgba [242 185  13 128] :title "Vibrant Yellow Stained Glass"}
   {:id 95  :rgba [108  86  19 128] :title "Dull Yellow Stained Glass"}
   {:id 96  :rgba [128 242  13 128] :title "Vibrant Green Stained Glass"}
   {:id 97  :rgba [ 64 108  19 128] :title "Dull Green Stained Glass"}
   {:id 98  :rgba [ 13 242 154 128] :title "Vibrant Teal Stained Glass"}
   {:id 99  :rgba [ 19 108  74 128] :title "Dull Teal Stained Glass"}
   {:id 100 :rgba [ 13  70 242 128] :title "Vibrant Blue Stained Glass"}
   {:id 101 :rgba [ 19  41 108 128] :title "Dull Blue Stained Glass"}
   {:id 102 :rgba [127  13 242 128] :title "Vibrant Purple Stained Glass"}
   {:id 103 :rgba [ 64  19 108 128] :title "Dull Purple Stained Glass"}
   {:id 104 :rgba [242  13 185 128] :title "Vibrant Pink Stained Glass"}
   {:id 105 :rgba [108  19  86 128] :title "Dull Pink Stained Glass"}
   {:id 106 :rgba [247 171 171 200] :title "Red Tinted Glass"}
   {:id 107 :rgba [247 228 171 200] :title "Yellow Tinted Glass"}
   {:id 108 :rgba [180 247 171 200] :title "Green Tinted Glass"}
   {:id 109 :rgba [171 247 247 200] :title "Teal Tinted Glass"}
   {:id 110 :rgba [171 209 247 200] :title "Blue Tinted Glass"}
   {:id 111 :rgba [209 171 247 200] :title "Purple Tinted Glass"}
   {:id 112 :rgba [247 171 228 200] :title "Pink Tinted Glass"}
   {:id 113 :rgba [255 255 255   0] :title "Unused"}])

(defn- to-html-color [[r g b a]]
  (str "rgba(" r "," g "," b "," (/ a 255.0) ")"))

(def static-colors (mapv #(assoc % :html (to-html-color (:rgba %))) base-colors))

(def static-color-rows
  {:grey-brown (mapv static-colors [1 2 3 4 5 6 7 64 65 66 67 68 69 70])
   :pale (mapv static-colors [57 58 59 60 61 62 63])
   :light (mapv static-colors [8 11 14 17 20 23 26 29 32 35 38 41 44 47])
   :normal (mapv static-colors [9 12 15 18 21 24 27 30 33 36 39 42 45 48])
   :dark (mapv static-colors [10 13 16 19 22 25 28 31 34 37 40 43 46 49])
   :deep (mapv static-colors [50 51 52 53 54 55 56])
   :grey-glass (mapv static-colors [71 72 73 74 75 76 77])
   :smoked-glass (mapv static-colors [78 79 80 81 82 83 84 85 86 87 88 89 90 91])
   :stained-glass (mapv static-colors [92 93 94 95 96 97 98 99 100 101 102 103 104 105])
   :tinted-glass (mapv static-colors [106 107 108 109 110 111 112])})


;;; MoonCats

(def +mooncat-border-color-index+ 114)

(defn- process-moon-cat-color [id name rgb a]
  {:id id
   :rgba (conj rgb (util/floor (* a 255)))
   :title (str "MoonCat " name)
   :html (str "rgba("(rgb 0)","(rgb 1)","(rgb 2)", "a")")})

(defn get-mooncat-palette-row [cat-id]
  (let [[_ border pattern body belly nose c1 c2] (color/cat-id->palette cat-id)]
    [(assoc (process-moon-cat-color 114 "Border (glows)" border 1) :glow (str "rgb(" (str/join "," (color/cat-id->glow cat-id)) ")"))

     (process-moon-cat-color 115 "Pattern" pattern 1)
     (process-moon-cat-color 116 "Coat" body 1)
     (process-moon-cat-color 117 "Belly/Whiskers" belly 1)
     (process-moon-cat-color 118 "Nose/Ears/Feet" nose 1)
     (process-moon-cat-color 119 "Eyes" border 1)

     (process-moon-cat-color 120 "Complement 1" c1 1)
     (process-moon-cat-color 121 "C1 Smoked Glass" c1 0.5)
     (process-moon-cat-color 122 "C1 Stained Glass" c1 0.4)
     (process-moon-cat-color 123 "C1 Tinted Glass" c1 0.3)

     (process-moon-cat-color 124 "Complement 2" c2 1)
     (process-moon-cat-color 125 "C2 Smoked Glass" c2 0.5)
     (process-moon-cat-color 126 "C2 Stained Glass" c2 0.4)
     (process-moon-cat-color 127 "C2 Tinted Glass" c2 0.3)
     ]))

(defn get-full-palette [cat-id]
  (into static-colors (get-mooncat-palette-row cat-id)))
