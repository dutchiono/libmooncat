(ns libmooncat.jslib
  (:require
   [libmooncat.core :as libmooncat]
   [libmooncat.data.contracts :as contracts]
   [libmooncat.data.resources :refer [+is-limited+]]
   [libmooncat.util :as util]))

(defn ^:export parseCatId [cat-id-or-rescue-order]
  (let [cat-id (libmooncat/parse-cat-id cat-id-or-rescue-order)]
    (when (string? cat-id)
      cat-id)))

(defn ^:export getCatId [rescue-order]
  (libmooncat/get-cat-id rescue-order))

(defn ^:export getRescueOrder [cat-id]
  (libmooncat/get-rescue-order cat-id))

(defn ^:export getTraits
  [result-type cat-id-or-rescue-order]
  (clj->js (libmooncat/get-traits result-type cat-id-or-rescue-order) :keyword-fn util/camel-case-keyword))

(defn- assoc-alt [m k alt-k]
  (if (contains? m k)
    m
    (if (contains? m alt-k)
      (assoc m k (get m alt-k))
      m)))

(defn- alt-keys [m]
  (-> m
      (assoc-alt :z-index :zIndex)
      (assoc-alt :palette-index :paletteIndex)
      (assoc-alt :background-color :backgroundColor)
      (assoc-alt :full-size :fullSize)
      (assoc-alt :no-cat :noCat)
      (assoc-alt :mirror-placement :mirrorPlacement)
      (assoc-alt :mirror-accessory :mirrorAccessory)))

(def audiences ["Everyone" "Teen" "Mature" "Adult"])

(defn ^:export parseAccessoryMeta [i]
  (let [m (libmooncat/parse-metabyte i)]
    (clj->js m :keyword-fn util/camel-case-keyword)))

(defn ^:export generateImage
  ([cat-id]
   (generateImage cat-id nil nil))
  ([cat-id accessories-js]
   (generateImage cat-id accessories-js nil))
  ([cat-id accessories-js options-js]
   (let [accessories (map alt-keys (js->clj accessories-js :keywordize-keys true))
         options (alt-keys (js->clj options-js :keywordize-keys true))]
     (libmooncat/generate-image cat-id accessories options))))

(defn ^:export fullPalette
  ([]
   (fullPalette "0xff00000ca7"))
  ([cat-id]
   (clj->js (libmooncat/get-full-accessory-palette cat-id))))

(defn- assoc-kw-prop [attributes kw default]
  (if-let [str-val (attributes (name kw))]
    (assoc attributes kw (keyword str-val))
    (assoc attributes kw default)))
(defn- assoc-hue-prop [attributes]
  (assoc attributes :hue
         (if (number? (attributes "hue"))
           (attributes "hue")
           (condp = (attributes "hue")
             "red" 0
             "orange" 30
             "yellow" 60
             "chartreuse" 90
             "green" 120
             "teal" 150
             "cyan" 180
             "sky-blue" 210
             "blue" 240
             "purple" 270
             "magenta" 300
             "fuchsia" 330
             "black" -1
             "white" -2
             -1))))
(defn assoc-pale-prop [attributes]
  (if (or (attributes "pale") (attributes "isPale"))
    (assoc attributes :pale true)
    attributes))

(defn ^:export generateMoonCatId [attributes-js]
  (let [attributes (-> (js->clj attributes-js)
                       (assoc-hue-prop)
                       (assoc-pale-prop)
                       (assoc-kw-prop :expression :smiling)
                       (assoc-kw-prop :pattern :pure)
                       (assoc-kw-prop :pose :standing)
                       (assoc-kw-prop :facing :left))]
    (libmooncat/generate-cat-id attributes)))

(defn ^:export randomMoonCatId
  ([] (libmooncat/random-cat-id))
  ([constraints]
   (let [constraints (->> constraints
                          js->clj
                          (reduce-kv (fn [acc k v]
                                       (if (vector? v)
                                         (assoc acc (keyword k) (mapv (fn [x] (if (string? x) (keyword x) x)) v))
                                         acc))
                                     {}))]
     (libmooncat/random-cat-id constraints))))

(defn ^:export getMoonCatIdByRescueOrder [rescueOrder]
  (when (and (int? rescueOrder) (<= 0 rescueOrder 25439))
    (libmooncat/get-cat-id rescueOrder)))

(defn ^:export rescueOrdersToEligibleList [rescueOrders]
  (clj->js (libmooncat/generate-eligible-list (js->clj rescueOrders))))

(defn ^:export eligibleListToRescueOrders [eligibleList]
  (clj->js (libmooncat/parse-eligible-list (js->clj eligibleList))))

(defn ^:export isEligibleListActive [eligibleList]
  (clj->js (libmooncat/eligible-list-active? (js->clj eligibleList))))

(defn ^:export activateEligibleList [eligibleList]
  (clj->js (libmooncat/activate-eligible-list (js->clj eligibleList))))

(defn ^:export deactivateEligibleList [eligibleList]
  (clj->js (libmooncat/deactivate-eligible-list (js->clj eligibleList))))

(defn ^:export isEligible [eligibleList rescueOrder]
  (libmooncat/eligible? eligibleList rescueOrder))

(defn ^:export filterRescueOrders [& filter-objects]
  (clj->js (apply libmooncat/filter-rescue-orders (map #(js->clj % :keywordize-keys true) filter-objects))))

(defn ^:export getContractDetails [rpc-url cat-id-or-rescue-order]
  (new js/Promise
       (fn [resolve reject]
         (.catch
          (.then (libmooncat/mooncat-contract-details rpc-url cat-id-or-rescue-order)
                 (fn [res]
                   (resolve (clj->js res :keyword-fn util/camel-case-keyword))))
          (fn [err] (reject err))))))

(defn ^:export isAcclimated [rpc-url cat-id-or-rescue-order]
  (libmooncat/acclimated? rpc-url cat-id-or-rescue-order))

(defn ^:export getTotalAccessories [rpc-url]
  (libmooncat/get-total-accessories rpc-url))

(defn ^:export getAccessory [rpc-url accessory-id]
  (new js/Promise
       (fn [resolve reject]
         (.catch
          (.then (libmooncat/get-accessory rpc-url accessory-id)
                 (fn [res] (resolve (clj->js res :keyword-fn util/camel-case-keyword))))
          (fn [err] (reject err))))))

(defn ^:export getTotalManagedAccessories [rpc-url manager-address]
  (libmooncat/get-total-managed-accesories rpc-url manager-address))

(defn ^:export getManagedAccessoryIdByIndex [rpc-url manager-address index]
  (libmooncat/get-managed-accessory-id-by-index rpc-url manager-address index))

(defn ^:export getTotalMoonCatAccessories [rpc-url rescue-order]
  (libmooncat/get-total-mooncat-accessories rpc-url rescue-order))

(defn ^:export getMoonCatAccessory [rpc-url rescue-order owned-accessory-index]
  (new js/Promise
       (fn [resolve reject]
         (.catch
          (.then (libmooncat/get-mooncat-accessory rpc-url rescue-order owned-accessory-index)
                 (fn [res] (resolve (clj->js res :keyword-fn util/camel-case-keyword))))
          (fn [err] (reject err))))))

(defn ^:export getDrawableMoonCatAccessory [rpc-url rescue-order owned-accessory-index]
  (new js/Promise
       (fn [resolve reject]
         (.catch
          (.then (libmooncat/get-drawable-mooncat-accessory rpc-url rescue-order owned-accessory-index)
                 (fn [res] (resolve (clj->js res :keyword-fn util/camel-case-keyword))))
          (fn [err] (reject err))))))

(def ^:export exports #js{"version" libmooncat/+version+
                          "totalMoonCats" libmooncat/+total-mooncats+
                          "parseCatId" parseCatId
                          "getCatId" getCatId
                          "getRescueOrder" getRescueOrder
                          "getTraits" getTraits
                          "generateImage" generateImage
                          "parseAccessoryMeta" parseAccessoryMeta
                          "fullPalette" fullPalette
                          "generateMoonCatId" generateMoonCatId
                          "randomMoonCatId" randomMoonCatId
                          "getMoonCatIdByRescueIndex" getMoonCatIdByRescueOrder
                          "rescueOrdersToEligibleList" rescueOrdersToEligibleList
                          "eligibleListToRescueOrders" eligibleListToRescueOrders
                          "isEligible" isEligible
                          "isEligibleListActive" isEligibleListActive
                          "activateEligibleList" activateEligibleList
                          "deactivateEligibleList" deactivateEligibleList
                          "filterRescueOrders" filterRescueOrders
                          "limited" +is-limited+
                          "contracts" contracts/all-js
                          "getContractDetails" getContractDetails
                          "isAcclimated" isAcclimated
                          "getTotalAccessories" getTotalAccessories
                          "getAccessory" getAccessory
                          "getTotalManagedAccessories" getTotalManagedAccessories
                          "getManagedAccessoryIdByIndex" getManagedAccessoryIdByIndex
                          "getTotalMoonCatAccessories" getTotalMoonCatAccessories
                          "getMoonCatAccessory" getMoonCatAccessory
                          "getDrawableMoonCatAccessory" getDrawableMoonCatAccessory
                          "println" println
                          })

(when js/window
  (js* "(~{}[~{}] = ~{})" js/window "LibMoonCat" exports))
