(ns libmooncat.filter
  (:require
   [clojure.string :as str]
   [libmooncat.data.resources :as resources]
   [libmooncat.data.color :as color]
   [libmooncat.traits :as traits]
   [libmooncat.util :as util]))

;;;;;

(defrecord MoonCatFilterable [^String id
                              ^long rescue-order
                              ^long vintage

                              ^boolean genesis
                              ^boolean pale
                              ^byte facing
                              ^byte expression
                              ^byte pattern
                              ^byte pose
                              ^int r
                              ^int g
                              ^int b
                              ^double hue
                              color])

(defn bin->byte [s]
  #?(:clj (Byte/parseByte s 2)
     :cljs (js/parseInt s 2)))

(defn hue->color [hue]
  (condp >= hue
    -2 :white
    -1 :black
    15.0 :red
    45.0 :orange
    75.0 :yellow
    105.0 :chartreuse
    135.0 :green
    165.0 :teal
    195.0 :cyan
    225.0 :sky-blue
    255.0 :blue
    285.0 :purple
    315.0 :magenta
    345.0 :fuchsia
    :red))

(defn mooncat-filterable [rescue-order cat-id]
  (let [{:keys [glow genesis pale hue-value k-int]} (traits/get-base-traits cat-id)
        k-bin (util/byte->bin k-int)]

    (MoonCatFilterable.
     cat-id
     rescue-order
     (traits/get-rescue-year rescue-order)
     genesis
     pale
     (bin->byte (subs k-bin 1 2))
     (bin->byte (subs k-bin 2 4))
     (bin->byte (subs k-bin 4 6))
     (bin->byte (subs k-bin 6 8))
     (glow 0)
     (glow 1)
     (glow 2)
     hue-value
     (color/hue->color-key hue-value)
     )))

(def filterables (vec (map-indexed mooncat-filterable resources/rescue-order->cat-id)))

;;;;


(defn boolean-predicate [key value]
  (when (boolean? value) #(= (key %) value)))

(defn integer-predicate [key value]
  (cond
    (nil? value) nil
    (fn? value) #(value (key %))
    (int? value) #(= (key %) value)
    (and (coll? value) (not (empty? value))) (let [values (set value)]
                                               #(contains? values (key %)))))

(defn set-predicate [mapping key value]
  (let [value (->> (if (coll? value)
                     (map mapping value)
                     [(mapping value)])
                   (remove nil?)
                   set)]
    (when-not (empty? value)
      #(contains? value (key %)))))

(def +vintages+ {2017 2017 2018 2018 2019 2019 2020 2020 2021 2021})
(def +facings+ {:left 0, :right 1, "left" 0, "right" 1, 0 0, 1 1})
(def +expressions+ {:smiling 0, :grumpy 1, :pouting 2, :shy 3, "smiling" 0, "grumpy" 1, "pouting" 2, "shy" 3, 0 0, 1 1, 2 2, 3 3})
(def +patterns+ {:pure 0, :tabby 1, :spotted 2, :tortie 3, "pure" 0, "tabby" 1, "spotted" 2, "tortie" 3, 0 0, 1 1, 2 2, 3 3})
(def +poses+ {:standing 0, :sleeping 1, :pouncing 2, :stalking 3, "standing" 0, "sleeping" 1, "pouncing" 2, "stalking" 3, 0 0, 1 1, 2 2, 3 3})

(defn- generate-filters [{:keys [id order vintage genesis pale facing expression pattern pose hue color]}]
  (let [id-filter (cond
                    (fn? id) #(id (:id %))
                    (string? id) #(= (:id %) id)
                    (coll? id) #(contains? id %))
        hue-filter (cond
                     (fn? hue) #(hue (:hue %)))
        order-filter (integer-predicate :rescue-order order)
        vintage-filter (set-predicate +vintages+ :vintage vintage)
        genesis-filter (boolean-predicate :genesis genesis)
        pale-filter (boolean-predicate :pale pale)
        facing-filter (set-predicate +facings+ :facing facing)
        expression-filter (set-predicate +expressions+ :expression expression)
        pattern-filter (set-predicate +patterns+ :pattern pattern)
        pose-filter (set-predicate +poses+ :pose pose)

        colors (->> (cond
                      (nil? color) nil
                      (coll? color) color
                      color [color])
                    (map keyword)
                    (remove nil?)
                    set)
        color-filter (when-not (empty? colors)
                       #(contains? colors (:color %)))
        filters (remove nil? [id-filter  hue-filter order-filter vintage-filter genesis-filter pale-filter facing-filter
                              expression-filter pattern-filter pose-filter color-filter])]
    (if (empty? filters)
      (constantly true)
      (apply every-pred filters))))

(defn- filter-mooncats- [rescue-orders! filter-map]
  (let [filter-fn (generate-filters filter-map)]
    (reduce (fn [res mcf]
              (if (filter-fn mcf)
                (conj! res (:rescue-order mcf))
                res))
            rescue-orders!
            filterables)))

(defn filter-mooncats [filter-maps]
  (sort
   (persistent!
    (reduce filter-mooncats- (transient #{}) filter-maps))))
