(ns libmooncat.ethereum.macros
  (:require
   [clojure.string :as str]
   [libmooncat.ethereum.eth-util :refer [encode-uint left-pad-hex-to-32-bytes right-pad-hex-to-32-bytes]]
   [libmooncat.util :refer [clean-hex-prefix]])
  (:import
   [org.web3j.utils Numeric Convert Convert$Unit]
   [org.web3j.crypto Hash]))

(def contract-input-arg-processors
  {:bytes5 ["bytes5" `clean-hex-prefix]

   :uint8 ["uint8" `(partial encode-uint 8)]
   :uint16 ["uint16" `(partial encode-uint 16)]
   :uint72 ["uint72" `(partial encode-uint 72)]
   :uint256 ["uint256" `(partial encode-uint 256)]

   :address ["address" `(comp left-pad-hex-to-32-bytes clean-hex-prefix)]


   ;;:cat-id ["bytes5" `clean-hex-prefix]

   ;;:token-id ["uint256" `(partial encode-uint 256)]
   ;;:token-index ["uint256" `(partial encode-uint 256)]
   ;;:wrapped-token-id ["uint256" `(partial encode-uint 256)]
   })


(defn keccak256 [message]
  (Numeric/toHexStringNoPrefix (Hash/sha3 (.getBytes message "UTF-8"))))

(defn- get-function-selector [fname arg-types]
  (subs (keccak256 (str fname "(" (str/join "," arg-types) ")")) 0 8))

(defmacro function-selector [fname & arg-types]
  (get-function-selector fname arg-types))

(defmacro contract-function-data-builder [contract-function-name & arg-specs]
  (let [arg-types (map (comp contract-input-arg-processors first) arg-specs)
        arg-type-strings (map first arg-types)
        arg-type-preps (map second arg-types)

        param-syms (mapv second arg-specs)

        arg-val-generators (mapv (fn [sym prep-fn]
                                   `(right-pad-hex-to-32-bytes (~prep-fn ~sym)))
                                 param-syms arg-type-preps)

        function-selector (get-function-selector contract-function-name arg-type-strings)
        ]
    `(fn [{:keys ~param-syms}]
       (let [arg-vals# ~arg-val-generators]
         (str "0x" ~function-selector (str/join arg-vals#))))))

(defmacro with-result-data [[data-sym rpc-result] & body]
  `(let [res# ~rpc-result]
     (if-let [~data-sym (:result res#)]
       (do ~@body)
       (throw (ex-info "request failed" res#)))))
