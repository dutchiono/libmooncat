# LibMoonCat javascript library

The library `libmooncat.js` contains helper functions for MoonCat Developers. It is designed for use in the browser.

All functions are available in the global `LibMoonCat` object.

You can download the latest build [here](https://gitlab.com/ponderware/libmooncat/-/jobs/artifacts/master/download?job=libmooncat-dist)

## Variants

There are two variants of the library, both share the **same API**. The difference is in how much data is embedded in the javascript file.

- `libmooncat.js` contains all data

- `libmooncat-limited.js` excludes certain data: *(impacts output from `getTraits`)*
  - limited group data - only group ids (e.g. `litterId`) are available
  - no rescuer addresses

## Properties

### `MoonCatLib.limited`

boolean - true if using limited version of the library

### `MoonCatLib.contracts`

object - contains **addresses** and JSON **abis** of deployed MoonCat Contract

## Functions

### parseCatId

Takes a `rescueOrder` as a string or an integer, or a `catId` string and returns a well-formed `catId` string if possible, or null if not

`parseCatId(integer rescueOrder | string rescueOrder | string catId)`

### getCatId

Takes a `rescueOrder` integer and returns a MoonCat's `catId` hex

`getCatId(int rescueOrder)`


### getRescueOrder

Takes a `catId` and returns the MoonCat's `rescueOrder` (if applicable)

`getRescueOrder(string catId)`

### getTraits

Takes a `resultType` and a `catId` or `rescueOrder` and returns a `traits` object

`getTraits(string resultType, string catId || int rescueOrder)`

available `resultTypes` are:

```
"basic" - basic MoonCat traits
"extended" - basic MoonCat traits with additional type and grouping data
"erc721" - ERC721 style traits
```

If the inputs are invalid it will return an object like:
```
{
    "classification": "invalid"
    "reason": ...
}
```

### generateImage

Takes a hex MoonCatId `catId`, an optional array of `accessories`, and an optional `options` object. Returns a PNG Data URI

`generateImage(string catId, array accessories, object options)`

Each accessory in the `accessories` array should be an object with the properties:

```
{
    idat: string the idat image data
    palettes: array of palettes (palette is an array of integer palette indexes)
    zIndex: integer layer distance from cat (+/- determined by background metadata)
    positions: 4 el array of 2 el arrays with x&y offsets for each position
    width: integer width of the accessory
    height: integer height of the accessory
    meta: integer metadata of the accessory

    glow: (optional) boolean when set, draw glow where applicable (default:true)
    paletteIndex: (optional) integer palette index to use (default: 0)
    background: (optional) boolean overrides meta attribute
    mirrorPlacement: (optional) boolean overrides meta attribute
    mirrorAccessory: (optional) boolean overrides meta attribute

}
```

The available properties for the `options` object are:

```
{
    scale: integer number of drawn pixels per accessory pixel (default: 1)
    padding: integer number of pixels to pad the drawn image (default: 3 * scale)
    backgroundColor: string background fill (default: transparent)
    fullSize: boolean, when set draws full 128x128 image instead of minimum bounding image (default: false)
    noCat: boolean, when true cat image is not included in output
}
```

Note: **a cat-id is required even if the `noCat` option is `true`** because the cat-id is used to derive the palette

### parseAccessoryMeta

Takes a single byte unsigned integer representing accessory metadata and returns an object.

`parseAccessoryMeta(uint8 meta)`

The properties on the return object are:

```
{
    verified: boolean
    reserved: integer (reserved for future use)
    audience: integer audience appropriateness
    audienceString: string representation of audience value
    mirrorPlacement: boolean, indicates if accessory placement should be mirrored
    mirrorAccessory: boolean, indicates if accessory pixel data should be mirrored
    background: boolean, indicates if accessory should be drawn behind the MoonCat
}

```

### generateMoonCatId

Takes an `attributes` object and return a hex MoonCatId

`generateMoonCatId(object attributes)`

The `attributes` map accepts the following keys:

```
{
    hue: -2 <= integer < 360 | string colorName (e.g. "chartreuse")
    expression: "smiling" | "grumpy" | "pouting" | "shy"
    pattern: "pure" | "tabby" | "spotted" | "tortie"
    pose: "standing" | "sleeping" | "pouncing" | "stalking"
    facing: "left" | "right"
    pale: boolean
}
```

Any missing keys in the `attributes` map will use the first option listed above

### randomMoonCatId

Takes an optional object of constraints and generates a random valid MoonCatId

`randomMoonCatId()` | `randomMoonCatId(object constraints)`

A the values of a constraint object are **arrays** of restrictions. Any missing key is unrestricted.

recognized keys:

```
{
    hues: [color names &| -2 <= integer < 360]
    expressions: ["smiling" &| "grumpy" &| "pouting" &| "shy"]
    patterns: ["pure" &| "tabby" &| "spotted" &| "tortie"]
    poses: ["standing" &| "sleeping" &| "pouncing" &| "stalking"]
    facings: ["left" &| "right"]
    pales: [true &| false]
}
```

### getMoonCatIdByRescueOrder

Takes an integer `rescueOrder` and returns that MoonCat's hex id (or null if the rescueOrder is out of bounds

`getMoonCatIdByRescueOrder(integer rescueOrder)`

### fullPalette

Returns an array of the full MoonCatAccessories palette for a supplied MoonCatId

`fullPalette(string catId)`

Each entry in the array is an object with the properties:

```
{
    id: integer palette index (should match array index)
    title: string human readable color name
    html: string in the form "rgba(r,g,b,a)"
    rgba: array of byte values in the form [r, g, b, a] where all values are between 0 & 255 inclusive
}
```

### rescueOrdersToEligibleList

Takes an array of rescueOrder integer and returns an array of 100 hex-encoded bytes32 values representing an eligible list

**Note: Eligible list will not be active by default, to activate it you must use the result from calling the `activateEligibleList`. If you are submitting an accessory to the smart contract with an eligibleList, the contract itself **will activate it**

`rescueOrdersToEligibleList(array rescueOrders)`

### eligibleListToRescueOrders

Takes an eligible list (array of 100 hex-encoded bytes32 values) and return an array of rescueOrders

`eligibleListToRescueOrders(array eligibleList)`

### activateEligibleList

Sets the active bit on an eligibleList

`activateEligibleList(array eligibleList)`

### deactivateEligibleList

Clears the active bit on an eligibleList

`deactivateEligibleList(array eligibleList)`


### isEligibleListActive

Takes an eligible list (array of 100 hex-encoded bytes32 values) and returns an array of rescueOrders

`isEligibleListActive(array eligibleList)`

### isEligible

Takes an eligible list (array of 100 hex-encoded bytes32 values) and a rescueOrder and returns a boolean
Note: If the eligible list is not active, it will return true for any rescueOrder

`isEligible(array eligibleList, integer rescueOrder)`

### filterRescueOrders

Takes one or multiple `filter` objects and returns an array of rescueOrders. If multiple `filter` objects are supplied, the results will be combined as though with **or**

`filterRescueOrders(object filter... filters)`

Each filter object can specify multiple criteria that the MoonCat must meet. The available properties (criteria) are:

```
{
    id: fn(string cat-id) | string | array of strings
    hue: fn(float hue)
    order: fn(integer rescue-order) | integer | array of rescue orders
    vintage: integer | array of integers
    genesis: boolean
    pale: boolean
    facing: "left" | "right"
    expression: "smiling" | "grumpy" | "pouting" | "shy" | array of expressions
    pattern: "pure" | "tabby" | "spotted" | "tortie" | array of patterns
    pose: "standing" | "sleeping" | "pouncing" | "stalking" | array of poses
    color: "red" | "orange" | "yellow" | "chartreuse" | "green" | ... | "fuchsia" | array of colors
}
```

If a filter critera is an array, any value in the array is considered valid.

## Experimental Blockchain Querying

### getContractDetails

Takes an ethereum node rpcURL and a catId or rescueOrder and returns a promise which resolves to a `contract details` object

`getContractDetails(string rpcURL, string catId | integer rescueOrder)`

the `contract details` object will have the form

```
{
    catId: string catId
    catName: string | null
    isAcclimated: boolean
    owner: string address
    rescueOrder: integer
    contract: {
                  address: string address
                  description: string
                  tokenId: integer | null
                  capabilities: array of strings | null
                  warning: string | null}
              }
}
```

### isAcclimated

Takes an ethereum node rpcURL and a catId or rescueOrder and returns a promise which resolves to a boolean

`isAcclimated(string rpcURL, string catId | integer rescueOrder)`


### totalAccessories

Takes an ethereum node rpcURL and returns a promise which resolves to a STRING form of the integer total number of accessories in the smart contract

`totalAccessories(string rpcURL)`

### getAccessory

Takes an ethereum node rpcURL and returns a promise which resolves to an object containing the following keys:

```
{
    "totalSupply": integer
    "availableSupply": integer
    "name": string
    "manager": address
    "priceWei": string
    "priceEth": string
    "available": boolean
    "availablePalettes": integer
    "palettes": vec of palettes
    "positions": vec of positions
    "meta": integer
    "width": integer
    "height": integer
    "idat": string
    "eligibleList": vector of 100 bytes32s"
}
```

### getTotalManagedAccessories

Fetches the total number of accessories managed by an address

`getTotalManagedAccessories(string rpcURL, string manager-address)`

### getManagedAccessoryIdByIndex

Fetches the total number of accessories managed by an address

`getManagedAccessoryIdByIndex(string rpcURL, string manager-address, integer index)`


### getTotalMoonCatAccessories

Fetches the number of accessories owned by the MoonCat

`totalMoonCatAccessories(string rpcURL, integer rescueOrder)`

### getMoonCatAccessory

Fetches the MoonCat's owned accessory data

`getMoonCatAccessory(string rpcURL, integer rescueOrder, integer ownedAccessoryIndex)`

```
{
	accessoryId: string (integer)
	paletteIndex: integer
	zIndex: integer
}

```

### getDrawableMoonCatAccessory

Fetches the MoonCat's owned accessory data as well as the image data of the accessory

`getDrawableMoonCatAccessory(string rpcURL, integer rescueOrder, integer ownedAccessoryIndex)`

```
{
	accessoryId: string (integer)
	paletteIndex: integer
	zIndex: integer
	positions: vector of positions
	palettes: vector of palettes
	meta: integer
	width: integer
	height: integer
	idat: idat hex-string
}
```


---

## License

©2021 ponderware

AGPL v3
