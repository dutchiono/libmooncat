(defproject com.ponderware/libmooncat "0.2.5"
  :description "Clojure(script) library for interacting with the MoonCatCommunity ecosystem"
  :url "https://gitlab.com/ponderware/libmooncat"
  :license {:name "AGPL v3"
            :url "https://www.gnu.org/licenses/agpl-3.0.en.html"
            :distribution :repo
            :comments "©2021 ponderware"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.866"]
                 [cheshire "5.10.0"]
                 [cljsjs/pako "0.2.7-0"]
                 [com.jhlabs/filters "2.0.235-1"]
                 [http-kit "2.5.3"]
                 [org.web3j/core "4.8.4"]]
  :resource-paths ["resources"]
  :main ^:skip-aot libmooncat.core
  :plugins [[lein-cljsbuild "1.1.8"]]
  :cljsbuild
  {:builds [{:id "release"
             :source-paths ["src"]
             :jar true
             :compiler {:output-dir "target/js-full"
                        :preamble ["license.js"]
                        :main "libmooncat.jslib"
                        :output-to "dist-js/libmooncat.js"
                        :optimizations :advanced
                        :pretty-print true}}
            {:id "release-limited"
             :source-paths ["src"]
             :compiler {:output-dir "target/js-limited"
                        :preamble ["license.js"]
                        :main "libmooncat.jslib"
                        :output-to "dist-js/libmooncat-limited.js"
                        :optimizations :advanced
                        :pretty-print true}}
            {:id "dev"
             :source-paths ["src"]
             :compiler {:output-dir "target/js-dev"
                        :main "libmooncat.jslib"
                        :output-to "dist-js/libmooncat.js"
                        :optimizations :whitespace
                        :pretty-print true}}]}
  :target-path "target/%s"
  :clean-targets [:target-path "dist-js/libmooncat.js" "dist-js/libmooncat-limited.js"]
  :aliases {"build-js-libs" ["do"
                             "clean"
                             ["run" "gen-limited-resources.cljs"]
                             ["cljsbuild" "once" "release-limited"]
                             ["run" "gen-resources.cljs"]
                             ["cljsbuild" "once" "release"]]
            "js-dev" ["do"
                      "clean"
                      ["run" "gen-resources.cljs"]
                      ["cljsbuild" "auto" "dev"]]}
  :deploy-repositories [["clojars"  {:sign-releases false :url "https://clojars.org/repo"}]]
  :repl-options {:init-ns libmooncat.core})
